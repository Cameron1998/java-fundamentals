public interface Detaiable {

    public abstract String getDetails();
}
