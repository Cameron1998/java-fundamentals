public class CurrentAccount extends Account{

    public CurrentAccount() {
    }

    public CurrentAccount(double balance, String name) {
        super(balance, name);
    }

    @Override
    public void addIntrest() {
        setBalance(getBalance() *  1.4);
    }

}
