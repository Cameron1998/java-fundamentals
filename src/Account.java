public abstract class Account implements Detaiable {

    private double balance;
    private String name;

    static Double intrestRate;

    public Account() {
        this.name = "Cameron";
        this.balance = 50;
    }

    public Account(double balance, String name) {
        this.balance = balance;
        this.name = name;
    }

    public abstract void addIntrest();

    public boolean withdraw(double amount){
        if(amount <= getBalance()){
            setBalance(getBalance()-amount);
            return true;
        }else
            return false;
    }
     public void withdraw(){
        withdraw(20);
     }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Double getIntrestRate() {
        return intrestRate;
    }

    public static void setIntrestRate(Double intrestRate) {
        Account.intrestRate = intrestRate;
    }

    @Override
    public String getDetails() {
        return "" + this.name +": " + this.balance;
    }
}
