public class HomeInsurance implements Detaiable{
    double premium;
    double excess;
    double amountInsured;

    public HomeInsurance(double premium, double excess, double amountInsured) {
        this.premium = premium;
        this.excess = excess;
        this.amountInsured = amountInsured;
    }


    @Override
    public String getDetails() {
        return  "" + premium + " " + excess;
    }
}
