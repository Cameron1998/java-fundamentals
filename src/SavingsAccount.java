public class SavingsAccount extends Account{

    public SavingsAccount() {
    }

    public SavingsAccount(double balance, String name) {
        super(balance, name);
    }

    @Override
    public void addIntrest() {
       // super.addIntrest();
        setBalance(getBalance() *  1.1);
    }

}
