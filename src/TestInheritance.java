public class TestInheritance {
    public static void main(String[] args) {
        Account[] accounts = new Account[3];
        accounts[0] = new SavingsAccount(2,"Fred"  );
        accounts[1] = new SavingsAccount(4,"Tom");
        accounts[2] = new CurrentAccount(6, "Alan");

        for (int i = 0; i < 3; i++) {
            accounts[i].addIntrest();
            System.out.println(accounts[i].getName() + ": " + accounts[i].getBalance());

        }

    }
}
