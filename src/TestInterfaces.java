public class TestInterfaces {

    public static void main(String[] args) {
        Detaiable[] details = new Detaiable[3];
        details[0] = new SavingsAccount(2,"Fred"  );
        details[1] = new SavingsAccount(4,"Tom");
        details[2] = new HomeInsurance(2,3,8);

        for (int i = 0; i < 3; i++) {
            System.out.println(details[i].getDetails());

        }
    }
}
